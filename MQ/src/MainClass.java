import screens.*;
public class MainClass {
	static EndScreen ds;
	public static void main(String[] args){
        Board main_board = new Board();
		main_board.generateMapFromFile("/home/bruno/Documents/documentation/FEUP/LPRO/git/MQ/src/map/maps/map");
		while(Board.died == 0 && Board.win == 0){
			main_board.updateFrame();
		}
		main_board.setVisible(false);
		if(Board.died != 0) {
			ds = new EndScreen("defeat.png");
			//death_screen ds = new death_screen();
		}else if(Board.win != 0) {
			//win_screen ds = new win_screen();
			ds = new EndScreen("victory.png");
		}
	}
}
