package quests;

public abstract class Quest{
	public Boolean fullfilled;
	public String description;
	public int current_progress;
	public int complete_progress;
	public Quest(){
		fullfilled = false;
		description = "Default quest description";
		current_progress = 0;
		complete_progress = 0;
	}
	public void advanceQuest() {
		current_progress++;
		if(current_progress == complete_progress) {
			finishQuest();
		}
	}
	public void finishQuest() {
		
	}
}