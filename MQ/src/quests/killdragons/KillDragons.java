package quests.killdragons;

import java.util.ArrayList;

import map.structures.Door;
import quests.*;

public class KillDragons extends Quest{
	public ArrayList<QuestDragon> quest_dragons = new ArrayList<QuestDragon>();
	public Door door_to_open;
	public KillDragons(){
		description = "Kill all 3 dragons in order to proceed";
	}
	public void startQuest(Door init_door_to_open, ArrayList<QuestDragon> init_quest_elements) {
		door_to_open = init_door_to_open;
		quest_dragons = init_quest_elements;
		complete_progress = init_quest_elements.size();
		for(QuestDragon element : quest_dragons){
			element.startQuest();
		}
	}
	public void finishQuest() {
		door_to_open.transversable = 1;
	}
}