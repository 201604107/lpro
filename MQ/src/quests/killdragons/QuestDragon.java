package quests.killdragons;
import map.Map;
import map.actor.*;
//import quests.QuestObject;
import quests.Quest;

public class QuestDragon extends Dragon{
	Quest TheQuest;
	public QuestDragon(Quest init_TheQuest, int init_row, int init_col, Map init_map){
		super(init_row,init_col, init_map);
		TheQuest = init_TheQuest;
	}
	@Override
	public void onDeath(Actor killer) {
		TheQuest.advanceQuest();
		visible = 0;
		killer.inventory.addAll(inventory);
	}
	public void startQuest() {
		
	}
	
}

