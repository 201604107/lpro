package map.actor;

import java.util.ArrayList;

import map.*;
import map.item.Item;

public class Actor extends Placeable{
	public int next_row;
	public int next_col;
	
	public int max_health;
	public int current_health;
	//Attack module
	public int attack_row;
	public int attack_col;
	public int attack_damage;
	public Boolean[][] attack_range;
	
	public ArrayList<Item> inventory;

	
	public Actor(int init_row,int init_col, Map init_map_obj, String init_GameCharacter, int init_max_health){
		super(init_map_obj);
		cur_row = init_row;
		cur_col = init_col;
		map_obj = init_map_obj;
		Print_Character = init_GameCharacter;
		max_health = init_max_health;
		current_health = init_max_health;
		inventory = new ArrayList<Item>();
	}
	public void onDeath(Actor killer) {}
	
	public void getHit(int damage, Actor killer){
		if(current_health - damage < 0){
			current_health = 0;
		}else{
			current_health -= damage;
		}
		if(current_health == 0){
			alive = 0;
			transversable = 1;
			onDeath(killer);
		}
	}
	public void tryMove(){
		if(map_obj.canMove(next_row,next_col,this)){
			map_obj.remove(cur_row,cur_col);
			cur_row = next_row;
			cur_col = next_col;
			map_obj.add(cur_row,cur_col,Print_Character);
		}
	}
	public void tryAttack(){
		ArrayList<Placeable> targets = getAttackable();
		for(Placeable element : targets ){
			if(element.alive == 1 && !element.Print_Character.equals(Print_Character)) {
				Attack(element);
			}
		}
	}
	public void Attack(Placeable target){
		if(target instanceof Actor) {
			Actor attacked = (Actor)target;
			attacked.getHit(attack_damage,this);
		}
	}
	public Boolean hasItem(String identifier){
		for(Item inv_item : inventory){
			if(inv_item.Print_Character.equals(identifier)){
				return true;
			}
		}
		return false;
	}
	public void pickUp(Item picked_up_item){
		//System.out.println(picked_up_item.cur_row+" "+picked_up_item.cur_col);		
		inventory.add(picked_up_item);
	}
	/*
	Returns all "alive" Placeables in the attack area
	*/
	public ArrayList<Placeable> getAttackable(){
		ArrayList<Placeable> all_attacked = new ArrayList<Placeable>();
		for(int row = 0; row < attack_range.length; row++){
			for(int col = 0; col < attack_range[0].length; col++){
				if(attack_range[row][col] && !(row == attack_row && col == attack_col)){
					all_attacked.addAll(map_obj.elementsAtPosition(cur_row+row-attack_row,cur_col+col-attack_col));
				}
			}
		}
		return all_attacked;
	}
}