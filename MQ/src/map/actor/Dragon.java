package map.actor;
import map.*;

import java.util.concurrent.ThreadLocalRandom;

public class Dragon extends Actor{
	long last_move;
	public Dragon(int init_row,int init_col, Map init_map){
		super(init_row,init_col, init_map, "D",1);
		last_move = System.currentTimeMillis();
		attack_col= 1;
		attack_row = 1;
		attack_damage = 1;
		attack_range = new Boolean[][]{
			{false,true,false},
			{true,true,true},
			{false,true,false},
		};
	}
	public void onDeath(Actor killer) {
		killer.inventory.addAll(inventory);
		visible = 0;
	}
	public void move() {
		next_row = cur_row+ThreadLocalRandom.current().nextInt(-1, 2);
		next_col = cur_col+ThreadLocalRandom.current().nextInt(-1, 2);
		tryMove();
	}
	public void updateFrame(){
		if(alive == 1) {
			tryAttack();
			if(last_move+1000 < System.currentTimeMillis()) {
				move();
				last_move = System.currentTimeMillis();
			}
		}
	}
}