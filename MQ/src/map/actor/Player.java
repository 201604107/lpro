package map.actor;

import java.util.ArrayList;
import map.*;

public class Player extends Actor{
	int keyEvent;
	public Player(int init_row,int init_col, Map init_map){
		super(init_row,init_col, init_map, "H",1);
		keyEvent = 0;
		can_pick = 1;
		attack_col= 1;
		attack_row = 1;
		attack_damage = 1;
		attack_range = new Boolean[][]{
			{true,true,true},
			{true,true,true},
			{true,true,true},
		};
	}
	public void updateFrame() {
		if(keyEvent == 1){
			keyEvent = 0;
			tryMove();
		}
		if(hasItem("S")) {
			tryAttack();
		}
	}
	public void updateKeyevent(int keyCode) {
		//System.out.println(keyCode);
		next_row = cur_row;
		next_col = cur_col;
		System.out.println(keyCode);
		switch(keyCode){
			case 37://Left
				if(next_col>0){
					next_col--;
				}
				break;
			case 38://Up
				if(next_row>0){
					next_row--;
				}
				break;
			case 39://Right
				if(next_col<9){
					next_col++;
				}
				break;
			case 40://Bottom
				if(next_row<9){
					next_row++;
				}
				break;
		}
		keyEvent = 1;
	}
};