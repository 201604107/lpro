package map;
import java.util.ArrayList;

import javax.swing.*;
import map.actor.*;
import map.item.*;
import map.structures.*;

public class Map{
	public ArrayList<Placeable> present_obj_list = new ArrayList<Placeable>();
    public JLabel[][] printable_board = new JLabel[10][10];
    
    /*
    Get all elements at a given map position
    */
    public ArrayList<Placeable> elementsAtPosition(int row, int col){
        ArrayList<Placeable> ret_vec = new ArrayList<Placeable>();
        for(Placeable element : present_obj_list){
            if(element.cur_row == row && element.cur_col == col){
                ret_vec.add(element);
            }
        }
        return ret_vec;
    }
    //--------------------------- Movement ---------------------------
    public void findPath(int row, int col, Placeable moving) {
    	/*
    	 TODO
    	 Implement Dijskra 
    	 */
    }
    public boolean move(int row, int col, Placeable moving) {
    	ArrayList<Placeable> all_elements =  elementsAtPosition(row,col);
        boolean can_move = true;
        if(printable_board[row][col].getText().equals("X")){
            return false;
        }
        Actor p = (Actor)moving;
        for(Placeable element : all_elements){
        	
        }
    	return false;
    }
    public boolean bump(Placeable bumping, Placeable bumped) {
    	
    	return false;
    }
    public boolean transverse(Placeable transversing, Placeable transversed) {
    	
    	return false;
    }
    
    /*
    Searches for a specific object at a specific map location
    TO BE CHANGED TO A MORE COMPREHENSIVE SEARCH (String identifier is not ok)
    */
    public Placeable Exists(int row, int col, String Identifier){
        if(row < 0 || row > 9 || col < 0 || col > 9){
            return null;
        }else if(printable_board[row][col].getText().equals(Identifier)){
            /*
            Overlapped objects are hidden, because the ID is only the visible one
            Looks easy to change if need be
            */
            for(Placeable element : present_obj_list){
                if(element.cur_row == row && element.cur_col == col){
                    return element;
                }
            }
            /*
            Reaching here is problematic and means the board and objects are out of phase
            */
            return null;
        }else{
            return null;
        }
    }

    public boolean canMove(int row, int col, Placeable moving){
        ArrayList<Placeable> all_elements =  elementsAtPosition(row,col);
        boolean can_move = true;
        if(printable_board[row][col].getText().equals("X")){
            return false;
        }
        Actor p = (Actor)moving;
        for(Placeable element : all_elements){
            if(element.bumpable == 1){
                Door d = (Door)element;
                if(d.transversable != 1 && !d.canBump(p)){
                    can_move = false;
                }
            }else if(element.transversable == 0){
                can_move = false;
            }else if(element.can_be_picked == 1 && moving.can_pick == 1){
                /*
                Need to perform a better check for item and player class
                */
                Item i = (Item)element;
                p.pickUp(i);
                i.getPicked((Actor)p);
                //can_move = true;
            }
        }
        return can_move;
    }
    public void remove(int row, int col){
        printable_board[row][col].setText(" ");
    }
    public void add(int row, int col, String Print_Character){
        printable_board[row][col].setText(Print_Character);
    }
}
