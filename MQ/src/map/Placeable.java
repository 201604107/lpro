package map;
public class Placeable{
	public String Print_Character;
	public int cur_row;
	public int cur_col;
	public int can_pick;
	public int can_be_picked;
	public int transversable;
	public int bumpable;
	public int alive;
	public int visible;
	public Map map_obj;
	
	public Placeable(Map init_map_obj){
		transversable = 0;
		bumpable = 0;
		can_pick = 0;
		can_be_picked = 0;
		alive = 1;
		visible = 1;
		map_obj = init_map_obj;
	}
	public int bump(){
		return -1;
	}
	public void print(){
		if(visible == 1) {
			map_obj.printable_board[cur_row][cur_col].setText(Print_Character);	
		}
	}
	public String toString() {
		return Print_Character;
	}
	public void updateFrame() {
		
	}
}