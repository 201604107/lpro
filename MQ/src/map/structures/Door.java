package map.structures;

import map.*;
import map.actor.*;
public class Door extends Placeable{
	Map map_obj;
	public Door(int init_row,int init_col, Map init_map_obj){
		super(init_map_obj);
		cur_row = init_row;
		cur_col = init_col;
		bumpable = 1;
		Print_Character = "E";
		map_obj = init_map_obj;
	}
	public boolean canBump(Actor p){
		if(p.hasItem("K")){
			transversable = 1;
			return true;
		}else{
			return false;
		}
	}
	public void updateFrame() {
		
	}
}