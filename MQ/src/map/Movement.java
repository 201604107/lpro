package map;


interface Movement {
	Boolean Bump();
	Boolean Bumped();
	Boolean Transversing();
	Boolean BeingTransversed();
}