package map.item;

import map.*;
import map.actor.*;

public class Item extends Placeable{
	String name;
	Map map_obj;
	public Item(int init_row,int init_col, Map init_map_obj){
		super(init_map_obj);
		cur_row = init_row;
		cur_col = init_col;
		can_be_picked = 1;
		alive = 0;
		transversable = 1;
		Print_Character = "I";
		map_obj = init_map_obj;
	}
	public void getPicked(Actor p){
		visible = 0;
	}

}