package map.item;

import map.*;
import map.actor.*;

public class Sword extends Item{
	public Sword(int init_row,int init_col, Map init_map_obj){
		super(init_row, init_col, init_map_obj);
		Print_Character = "S";
	}
	public void getPicked(Actor p){
		visible = 0;
		p.Print_Character = "A";
	}
}