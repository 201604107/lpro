import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.swing.*;

import map.*;
import map.actor.*;
import map.item.*;
import map.structures.*;
import quests.killdragons.KillDragons;
import quests.killdragons.QuestDragon;

public class Board extends JFrame{
	private static final long serialVersionUID = 2195379760914381351L;
	int row,col;
	public static int died=0;
	public static int win=0;

	Map map_obj = new Map();
	
    JPanel main_panel=new JPanel();
    FlowLayout main_layout = new FlowLayout();
    JPanel secondary_panel=new JPanel();
	GridLayout game_grid = new GridLayout(10,10);
	
    Player main_player;
	Dragon main_dragon;
	Key key;
	Sword sword;
	Door main_door;

	KillDragons kill_dragons_quest = new KillDragons();
	ArrayList<QuestDragon> quest_dragons = new ArrayList<QuestDragon>();
	public Board() {
		main_player = new Player(1,1,map_obj);
		//main_dragon = new Dragon(3,1,map_obj);
		main_door = new Door(5,9,map_obj);
		sword = new Sword(8,1,map_obj);
		//key = new Key(0,0,map_obj);
		//key.visible = 0;
		
		//main_dragon.inventory.add(key);
		
		quest_dragons.add(new QuestDragon(kill_dragons_quest,3,1,map_obj));
		quest_dragons.add(new QuestDragon(kill_dragons_quest,3,4,map_obj));
		
		kill_dragons_quest.startQuest(main_door, quest_dragons);
		
		map_obj.present_obj_list.add(main_player);
		//map_obj.present_obj_list.add(main_dragon);
		map_obj.present_obj_list.add(sword);
		map_obj.present_obj_list.add(main_door);

		for(Placeable element : quest_dragons){
        	map_obj.present_obj_list.add(element);
        }
		
		setTitle("Here There Be Dragons");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(200, 200);

	    for(row = 0; row < 10; row++) {
    		for(col = 0; col < 10; col++) {
    			map_obj.printable_board[row][col] = new JLabel(" ");
    			secondary_panel.add(map_obj.printable_board[row][col]);
            }
        }	    
	    secondary_panel.setLayout(game_grid);
	    main_panel.setLayout(main_layout);
	    main_panel.add(secondary_panel);
	    main_panel.setVisible(true);
        add(main_panel);
	    setVisible(true);    
	    addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {
				main_player.updateKeyevent(e.getKeyCode());            	
            }
            @Override
            public void keyReleased(KeyEvent e) {}
        });
	}
	
	public void generateMapFromFile(String fileName) {
		int c;
		String temp_str_holder[][] = new String[10][10];
        try {
			col = 0;
			row = 0;
			FileReader inStream = new FileReader(fileName);
			while((c = inStream.read()) != -1) {
            	if(c==10) {
            		col=0;
            		row++;
            		continue;
            	}else {
            		temp_str_holder[row][col] = (char)c+"";
            		col++;
            	}
            }
            inStream.close();
			for(row = 0; row < 10; row++) {
				//map_obj.full_grid.add(row, temp_holder);
				for(col = 0; col < 10; col++) {
					map_obj.printable_board[row][col].setText(temp_str_holder[row][col]);
					//map_obj.external_game_grid[row][col].setText(map_obj.in_c[row][col]);
				}
			}
        }catch(Exception e) {
            System.out.println("Exception caught '" + fileName + "'");                
		}
        for(Placeable element : map_obj.present_obj_list){
        	element.print();
        }
	}

	public void updateFrame(){
		try{
			TimeUnit.MILLISECONDS.sleep(10);
			//TimeUnit.SECONDS.sleep(1);
		}catch(Exception e){

		}
		if(main_player.current_health == 0){
			died = 1;
		}
		if(main_player.cur_row == main_door.cur_row && main_player.cur_col == main_door.cur_col) {
			win = 1;
		}
		for(Placeable element : map_obj.present_obj_list){
        	element.updateFrame();
        }
        for(Placeable element : map_obj.present_obj_list){
        	element.print();
        }
	}
}
