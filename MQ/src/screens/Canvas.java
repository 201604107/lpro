package screens;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;

public class Canvas extends Panel {
	private static final long serialVersionUID = -8926850418303638954L;
	BufferedImage  image;
	 public Canvas(String imageName) {
		 try{
			imageName = "./src/screens/"+imageName;
			File input = new File(imageName);
			image = ImageIO.read(input);
		 }catch(IOException ie){
			System.out.println("Error:"+ie.getMessage());
		 }
	 }
	
	 public void paint(Graphics g) {
		 g.drawImage(image, 0, 0, null);
	 }
}
/*
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

public class canvas extends Canvas{
    private static final long serialVersionUID = 5703217428905757134L;
    public String MyImage;
    public canvas(String init_MyImage) {
    	MyImage = +init_MyImage;
    }
    public void paint(Graphics g) {
		Toolkit t=Toolkit.getDefaultToolkit();
		Image i=t.getImage(MyImage);
		g.drawImage(i, 120,100,this);
		
    }
}
*/